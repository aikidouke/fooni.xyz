+++
Categories = [ "Fedora", "systemd", "IPv6" ]
date = "2016-02-18T13:15:57-05:00"
Tags = [ "Fedora", "systemd", "IPv6" ]
draft = true
title = "IPv6 configuration with systemd on Fedora 23"
icon = "linux"
color = "gray"
+++

My ISP has started offering IPv6 address to customers. I expirement in and on virtual machines quite a bit and most comfortable with the kvm/qemu/virt-manager offering in Fedora/CentOS. I have wanted to get familiar with IPv6 and use it in my virtual machines. This is what worked for me.


### Setting up IPv6 with systemd-networkd


* Please start by running through this excellent tutorial on systemd-networkd by [Major Hayden](https://major.io/2015/03/26/creating-a-bridge-for-virtual-machines-using-systemd-networkd/)


### My IPv6 configuration files (with a few changes to the device name and IPs)


#### eth1.network


```bash
   [Match]
   Name=eth1

   [Network]
   Bridge=br0
```

#### br0.netdev


```bash
   [NetDev]
   Name=br0
   Kind=bridge
```

#### br0.network


```bash
    [Match]
    Name=br0

    [Network]
    DNS=8.8.8.8
    Address=192.168.100.100/24
    Gateway=192.168.100.1
    DHCP=ipv6
    IPv6PrivacyExtensions=yes
    DNS=2620:0:ccc::2
    DNS=2620:0:ccd::2
```

The first DNS is obviously google. The second two might be google or opendns, not sure, meant to change those. Feel free to contact me with comments or questions.


