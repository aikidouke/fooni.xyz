+++
date = "2016-02-18T13:04:53-05:00"
draft = true
title = "about"
Categories = [ "Hello.World" ]
Tags = [ "Hello.World" ]
color = "green"
icon = "user"

+++

## $foo - A term used for unimportant variables in programming when the programmer is too lazy to think of an actual name.

Topics related to unix-like operating systems interspersed with personal views and/or content. Comments welcome.
